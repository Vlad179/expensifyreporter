import json
import webapp2
from entities.Report import Report, Check
import dateutil.parser
from google.appengine.ext.db import BadArgumentError
from google.appengine.ext import ndb

__author__ = 'Bezhentsev Vladislav'


class ReportsRequestHandler(webapp2.RequestHandler):
    def post(self):
        user_id = self.request.get('user_id')
        report_name = self.request.get('name')
        check_keys = self.request.get('check_keys')

        try:
            user_id = int(user_id)
            if user_id <= 0:
                raise ValueError('The value of parameter \'user_id\' should be a positive integer')
            if not report_name:
                raise ValueError('The value of parameter \'name\' should be a non-empty string')
            check_keys = check_keys.split()
            check_keys = map(lambda key: ndb.Key(urlsafe=key), check_keys)
            check_keys = list(set(check_keys))  # Remove duplicates
        except ValueError:
            self.error(400)
            return

        report = Report(user_id=user_id, name=report_name, check_keys=check_keys)
        # Define the period of the report
        if check_keys:
            report.define_period()
        report.put()

        # Link this report to all checks that this report contains
        report_id = report.key.id()
        report.total = 0
        for check_key in check_keys:
            check = check_key.get()
            check.related_reports.append(report_id)
            check.put()
            report.total += check.total
        report.put()

    def get(self):
        reports = Report.query().order(-Report.last_date)
        user_id = self.request.get('user_id')
        start = self.request.get('start', 0)
        end = self.request.get('end', reports.count())
        start_date = self.request.get('start_date')
        end_date = self.request.get('end_date')

        try:
            user_id = int(user_id)
            if user_id <= 0:
                raise ValueError('The value of parameter \'user_id\' should be a positive integer')
            start = int(start)
            if start < 0:
                raise ValueError('The value of parameter \'start\' should be a non-negative integer')
            end = int(end)
            if end < 0:
                raise ValueError('The value of parameter \'end\' should be a non-negative integer')
            start_date = dateutil.parser.parse(start_date) if start_date else ''
            end_date = dateutil.parser.parse(end_date) if end_date else ''
        except ValueError:
            self.error(400)
            return
        try:
            reports = reports.fetch(offset=start, limit=end - start) if end > start else []
        except BadArgumentError:
            return
        reports = filter(lambda report: report.user_id == user_id, reports)
        if start_date != '':
            reports = filter(lambda report: report.last_date >= start_date, reports)
        if end_date != '':
            reports = filter(lambda report: report.last_date <= end_date, reports)

        response = json.dumps(map(lambda report: report.convert_to_dict(), reports))
        self.response.out.write(response)

    def put(self):
        report_id = self.request.get('report_id')
        report_name = self.request.get('name')
        check_keys = self.request.get('check_keys')

        try:
            report_id = int(report_id)
            check_keys = check_keys.split()
            check_keys = map(lambda key: ndb.Key(urlsafe=key), check_keys)
            check_keys = list(set(check_keys))  # Remove duplicates
        except ValueError:
            self.error(400)
            return

        report = Report.get_by_id(report_id)
        if report:
            # Remove all checks from the report
            report.total = 0
            for check_key in report.check_keys:
                check = check_key.get()
                check.related_reports.remove(report_id)
                check.put()
                report.check_keys.remove(check_key)

            # Add given checks to the report
            for check_key in check_keys:
                report.check_keys.append(check_key)
                check = Check.get_by_id(check_key.id())
                # Change period of the report if the check goes beyond the initial period
                if report.initial_date:
                    if report.initial_date > check.date:
                        report.initial_date = check.date
                else:
                    report.initial_date = check.date
                if report.last_date:
                    if report.last_date < check.date:
                        report.last_date = check.date
                else:
                    report.last_date = check.date

            if report_name:
                report.name = report_name

            # Link this report to all checks that this report contains
            for check_key in check_keys:
                check = check_key.get()
                check.related_reports.append(report_id)
                check.put()
                report.total += check.total
            report.put()

    def delete(self):
        report_id = self.request.get('report_id')

        try:
            report_id = int(report_id)
        except ValueError:
            self.error(400)
            return

        report = Report.get_by_id(report_id)
        if report:
            if report.check_keys:
                for check_key in report.check_keys:
                    check = check_key.get()
                    check.related_reports.remove(report_id)
                    check.put()
            report.key.delete()
            self.response.out.write(True)
