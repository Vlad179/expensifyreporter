from google.appengine.ext import ndb
from entities.Check import Check
import datetime

__author__ = 'Andrey Kudryavtsev, Vladislav Bezhentsev'


class Report(ndb.Model):
    user_id = ndb.IntegerProperty(required=True)
    name = ndb.StringProperty(required=True)
    initial_date = ndb.DateTimeProperty()
    last_date = ndb.DateTimeProperty()
    check_keys = ndb.KeyProperty(kind=Check, repeated=True)
    total = ndb.FloatProperty()  # Sum of totals of all checks related to this report
    is_in_expensify = ndb.IntegerProperty(default=0)
    is_in_concur = ndb.IntegerProperty(default=0)

    def convert_to_dict(self):
        d = self.to_dict()
        d['id'] = self.key.id()
        d['checks'] = map(lambda key: key.get().convert_to_dict(del_photo=False), self.check_keys)
        del d['check_keys']
        if self.initial_date:
            d['initial_date'] = d['initial_date'].isoformat()
        if self.last_date:
            d['last_date'] = d['last_date'].isoformat()
        if self.is_in_concur:
            d['in_concur'] = self.is_in_concur
        if self.is_in_expensify:
            d['in_expensify'] = self.is_in_expensify
        return d

    def remove_checks(self, check_keys):
        for check_key in check_keys:
            self.check_keys.remove(check_key)

    def define_period(self):
        if not self.initial_date:
            self.initial_date = datetime.datetime.now()
        if not self.last_date:
            self.last_date = datetime.datetime.min
        for check_key in self.check_keys:
            check = Check.get_by_id(check_key.id())
            if (check.date > self.last_date):
                self.last_date = check.date
            if (check.date < self.initial_date):
                self.initial_date = check.date

