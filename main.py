#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2

from handlers.ChecksRequestHandler import ChecksRequestHandler
from handlers.ReportsRequestHandler import ReportsRequestHandler
from handlers.ChecksRequestHandler import UploadHandler
from expensify_concurIntegration.ExpensifyRequestsHandler import ExpensifyUserHandler
from expensify_concurIntegration.ExpensifyRequestsHandler import ExpensifyReportHandler
from expensify_concurIntegration.ConcurRequestsHandler import ConcurUserHandler
from expensify_concurIntegration.ConcurRequestsHandler import ConcurReportHandler
from expensify_concurIntegration.ConcurAuthorization import ConcurAuthorization


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/checks', ChecksRequestHandler),
    ('/reports', ReportsRequestHandler),
    ('/upload', UploadHandler),
    ('/exp_user', ExpensifyUserHandler),
    ('/exp_send_report', ExpensifyReportHandler),
    ('/con_user', ConcurUserHandler),
    ('/con_send_report', ConcurReportHandler),
    ('/concur_info', ConcurAuthorization)

], debug=True)
