from google.appengine.api import urlfetch
import xml.etree.ElementTree
import datetime
import webapp2

from entities.ConcurUser import ConcurUser

CONST_CLIENT_ID = "GGLzZezceFfuqaHldooZ2B"
CONST_CLIENT_SECRET = "NqxFSzhenuyN2pq55jESOecE9pGbhUtK"

FAILED = "<html><body><h2>Authentication failed!\nTry again!</h2></body></html>"
SUCCESS = "<html><body><h2>Successful Authorization!</h2></body></html>"

class ConcurAuthorization(webapp2.RequestHandler):

    def get(self):
        code = self.request.get('code')
        user_id = int(self.request.get('state'))
        if code and user_id:
            user = ConcurUser.query(ConcurUser.user_id == user_id).get() #check whether the user is already in system
            if user:
              self.response.write('<html><body><h2>User has already the link with Concur!')
              self.response.write('</h2></body></html>')
              return
            url = "http://www.concursolutions.com/net2/oauth2/GetAccessToken.ashx?&code=" + code + \
                  "&client_id=" + CONST_CLIENT_ID +    "&client_secret=" + CONST_CLIENT_SECRET
            xmls = urlfetch.fetch(url=url).content #send to Concur to get access token
            self.response.write(xmls)
            try:
                tree = xml.etree.ElementTree.fromstring(xmls)
                if tree:
                    token = tree.find('Token').text
                    exp_date = tree.find('Expiration_date').text
                    refresh_token = tree.find('Refresh_Token').text
                    expiration_date = datetime.datetime.strptime(exp_date, "%m/%d/%Y %I:%M:%S %p")
                    user = ConcurUser.create_user(int(user_id), token, expiration_date, refresh_token)
                    if user is not None:
                        self.response.write(SUCCESS)
                else:
                    self.response.write(FAILED)
            except Exception:
                self.response.write(FAILED)
        else:
            self.response.write(FAILED)