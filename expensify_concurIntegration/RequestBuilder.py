# -*- coding: utf-8 -*-
from google.appengine.api import urlfetch
from google.appengine.ext import blobstore
import xml.etree.ElementTree
import datetime
import urllib
import json

from entities.ConcurUser import ConcurUser
from entities import ExpensifyUser
from entities import Check
from entities import Currency
from CategoryDefiner import CategoryDefiner


CONST_CLIENT_ID = "GGLzZezceFfuqaHldooZ2B"
CONST_CLIENT_SECRET = "NqxFSzhenuyN2pq55jESOecE9pGbhUtK"




class RequestBuilder:

    """
    partnerUserID - id of the user in Expensify
    partnerUserSecret - token of the user in Expensify
    title - name of the Report
    email - email of employee
    expenses - array of expenses(Check.py) that are the components of the report
    """
    @staticmethod
    def expensify_create_report(partnerUserID, partnerUserSecret, title, email, expenses, policyID):
        expensify_url = "https://integrations.expensify.com/Integration-Server/ExpensifyIntegrations?requestJobDescription="
        if policyID is None:
            result = ExpensifyUser.ExpensifyUser.create_policy(partnerUserID, partnerUserSecret)
            if (result.status_code == 200):
                policy_info = json.loads(result.content)
                policyID = policy_info["policyID"]
            else:
                raise ValueError("Cannor crate policyID")
        data = {"type":"create"}
        credentials = {}
        credentials['partnerUserID'] = partnerUserID
        credentials['partnerUserSecret'] = partnerUserSecret
        data["credentials"] = credentials
        inputSettings = { "expenses":[]}
        inputSettings['type'] = "report"
        inputSettings['policyID'] = policyID
        inputSettings['employeeEmail'] = email
        report = {}
        report['title'] = title
        inputSettings["report"] = report
        single_expense = {}
        for expense_key in expenses:
            expense = Check.Check.get_by_id(expense_key.id())
            if expense.date: single_expense["date"] = str(expense.date.date())
            if expense.currency: single_expense["currency"] = Currency.set_currency_for_integration(expense.currency)
            if expense.address:
                single_expense["merchant"] = expense.address
            else:
                single_expense["merchant"] = "No Merchant"
            if expense.total: single_expense["amount"] = int(expense.total*100)
            inputSettings["expenses"].append(single_expense)
            single_expense = {}
        data["inputSettings"] = inputSettings
        json_ = json.dumps(data)
        form_data = urllib.quote_plus(json_)
        url = '%s%s' % (expensify_url, form_data)
        result = urlfetch.fetch(url)
        return result.content

    @staticmethod
    def concur_create_report(user, title, date, expenses):
        content = RequestBuilder.concur_create_header_report(user.access_token, title, date)
        info = xml.etree.ElementTree.fromstring(content)
        url_for_expenses = info.findall('{http://www.concursolutions.com/api/'
                                        'expense/expensereport/2011/03}Report-Details-Url')[0].text
        headers = {'Content-Type': 'application/xml', 'Authorization': "oauth " + str(user.access_token)}
        content = urlfetch.fetch(url=str(url_for_expenses), payload=xml, headers= headers, method=urlfetch.GET).content
        url_for_expenses = "https://www.concursolutions.com/api/expense/expensereport/v1.1/report/"
        info = xml.etree.ElementTree.fromstring(content)
        report_id = info.findall('{http://www.concursolutions.com/api/expense/expensereport/2011/03}ReportId')[0].text
        url_for_expenses += report_id
        if expenses:
            content = RequestBuilder.concur_send_expenses(url_for_expenses, user.access_token, expenses)
        user.concur_report_ids.append(report_id)
        user.put()
        return content


    #create Report in Concur System
    @staticmethod
    def concur_create_header_report(access_token, title, date):
        url = "https://www.concursolutions.com/api/expense/expensereport/v1.1/report"
        headers = {'Content-Type': 'application/xml', 'Authorization': "oauth " + str(access_token)}
        xml = "<Report "
        xml += "xmlns=\"http://www.concursolutions.com/api/expense/expensereport/2011/03\"><Name>"
        xml += title.encode("utf-8")
        xml += "</Name><UserDefinedDate>" + str(date) + "</UserDefinedDate></Report>"
        result = urlfetch.fetch(url= url, payload=xml, headers= headers, method=urlfetch.POST)
        return result.content

    #fill created Report with expenses
    @staticmethod
    def concur_send_expenses(url, access_token, expenses):
        headers = {'Content-Type': 'application/xml', 'Content-Encoding' : 'unicode', 'Authorization': "oauth " + str(access_token)}
        xmls = "<ReportEntries "
        xmls += "xmlns=\"http://www.concursolutions.com/api/expense/expensereport/2011/03\">"
        for expense_key in expenses:
            expense = Check.Check.get_by_id(expense_key.id())
            cheque = unicode(expense.address).encode("utf-8").decode("utf-8")
            xmls += "<Expense>"
            if expense.currency:
                xmls += "<CrnCode>"+ Currency.set_currency_for_integration(expense.currency) +"</CrnCode>"
            if expense.address:
                xmls += "<VendorDescription>"+cheque+"</VendorDescription>"
            if expense.date:
                xmls += "<TransactionDate>"+str(expense.date.date())+"</TransactionDate>"
            if expense.total:
                xmls += "<TransactionAmount>"+str(expense.total) +"</TransactionAmount>"
            if expense.category:
                xmls += "<ExpKey>"+str(CategoryDefiner.define_category(str(expense.category), "concur")) +"</ExpKey>"
            if expense.note:
                xmls += "<Comment>" + unicode(expense.note).encode("utf-8").decode("utf-8") + "</Comment>"
            xmls += "<IsPersonal>N</IsPersonal>"
            xmls += "</Expense>"
        xmls += "</ReportEntries>"
        url = url + "/entry"
        xmls = xmls.encode('utf-8', 'ignore')

        result = urlfetch.fetch(url= url, payload=xmls, headers= headers, method=urlfetch.POST).content
        #now attach photos to the created expenses

        #get all ids of expenses in report
        tree = xml.etree.ElementTree.fromstring(result)
        for index, status in enumerate(tree.findall(
                '{http://www.concursolutions.com/api/expense/expensereport/2011/03}ReportEntryStatus')):
            expense_id = status.find(
                '{http://www.concursolutions.com/api/expense/expensereport/2011/03}Report-Entry-Details-Url').text[-36:]
            expense = Check.Check.get_by_id(expenses[index].id())
            if expense.photo:
                RequestBuilder.add_photo_to_expense(expense_id, expense, access_token)
        return


    @staticmethod
    def add_photo_to_expense(exp_id, expense, access_token):
        url = 'https://www.concursolutions.com/api/image/v1.0/expenseentry/' + exp_id
        blob_reader = blobstore.BlobReader(expense.photo)
        value = blob_reader.read()
        headers = {'Authorization': "oauth " + str(access_token), 'Content-Type': "image/jpeg",
                   'Content-Length': len(value)}
        result = urlfetch.fetch(url= url, payload= value, headers= headers, method=urlfetch.POST)
        content = result.content

    """
    update users list of reports, deleting all reports that are not in Concur
    """
    @staticmethod
    def get_all_user_reports_in_concur( concur_user):
        url = "https://www.concursolutions.com/api/expense/expensereport/v2.0/Reports/"
        headers = {'Content-Type': 'application/xml', 'Authorization': "oauth " + str(concur_user.access_token)}
        content = urlfetch.fetch(url= url, headers= headers, method=urlfetch.GET).content
        info = xml.etree.ElementTree.fromstring(content)
        report_summaries = info.findall('{http://www.concursolutions.com/api/expense/expensereport/2012/07}ReportSummary')
        concur_report_ids = []
        for report_summary in report_summaries:
            concur_report_ids.append(report_summary.find('{http://www.concursolutions.com/api/expense/expensereport/2012/07}ReportId').text)
        for index, user_report in enumerate(concur_user.concur_report_ids):
            if user_report not in concur_report_ids:
                concur_user.concur_report_ids.remove(user_report)
                concur_user.report_keys.pop(index)
        concur_user.put()

    """
    Refresh access token if it is expired
    """
    @staticmethod
    def refresh_access_token(user):
        url = 'https://www.concursolutions.com/net2/oauth2/getaccesstoken.ashx?' \
              'refresh_token=' + user.refresh_token + \
              '&client_id=' + CONST_CLIENT_ID +\
              '&client_secret=' + CONST_CLIENT_SECRET
        result = urlfetch.fetch(url= url, method=urlfetch.GET)
        content = result.content
        try:
            tree = xml.etree.ElementTree.fromstring(content)
            if tree is not None:
                token = tree.find('Token').text
                exp_date = tree.find('Expiration_date').text
                expiration_date = datetime.datetime.strptime(exp_date, "%m/%d/%Y %I:%M:%S %p")
                user.access_token = token
                user.expiration_date = expiration_date
                user.put()
                return True
        except Exception:
            return False