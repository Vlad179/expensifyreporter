
class CategoryDefiner():

    @staticmethod
    def define_category(category_name, sys_name):
        if category_name == "Airfare":
            if sys_name == "concur":
                return "AIRFR"
        if category_name == "Car Rental":
            if sys_name == "concur":
                return "CARRT"
        if category_name == "Hotel":
            if sys_name == "concur":
                return "LODNG"
        if category_name == "Fuel":
            if sys_name == "concur":
                return "GASXX"
        if category_name == "Parking":
            if sys_name == "concur":
                return "PARKG"
        if category_name == "Taxi":
            if sys_name == "concur":
                return "TAXIX"
        if category_name == "Tolls/Road Charges":
            if sys_name == "concur":
                return "TOLLS"
        if category_name == "Train":
            if sys_name == "concur":
                return "TRAIN"
        if category_name == "Breakfast":
            if sys_name == "concur":
                return "BRKFT"
        if category_name == "Business Meals":
            if sys_name == "concur":
                return "BUSML"
        if category_name == "Dinner":
            if sys_name == "concur":
                return "DINNR"
        if category_name == "Lunch":
            if sys_name == "concur":
                return "LUNCH"
        if category_name == "Office Supplies/Software":
            if sys_name == "concur":
                return "OFCSP"
        if category_name == "Internet/Online Fees":
            if sys_name == "concur":
                return "ONLIN"
        if category_name == "Mobile/Cellular Phone":
            if sys_name == "concur":
                return "CELPH"
        if category_name == "Miscellaneous":
            if sys_name == "concur":
                return "MISCL"
        return "MISCL"
