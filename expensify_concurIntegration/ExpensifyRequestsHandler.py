from google.appengine.ext import ndb
import webapp2
import json

from entities.ExpensifyUser import ExpensifyUser
from entities.Report import Report
import RequestBuilder


class ExpensifyReportHandler(webapp2.RequestHandler):
    """
    send report to Expensify when the user still is not in database
    json in body has to have 4 fields:  user_id,  report_id
    """
    def post(self):
        info = json.loads(unicode(self.request.body, "utf-8"))
        user_id = info["user_id"]
        report_id = info["report_id"]
        try:
            user_id = int(user_id)
            report_id = int(report_id)
        except ValueError:
            self.error(400)
            return
        user = ExpensifyUser.query(ExpensifyUser.user_id == user_id).get() #search for expensify user with user_id
        if user:
            report = Report.get_by_id(report_id) #check whether report with report_id in the system
            if report:
                report_key_list = user.report_keys
                if report_id not in report_key_list: #check whether report is already uploaded in Expensify
                    user.report_keys.append(report_id)
                    req = RequestBuilder.RequestBuilder()
                    result = req.expensify_create_report(user.expensify_user_id, user.expensify_user_secret,
                                                        report.name, user.user_email,report.check_keys, user.policy_id)
                    if "error" in result:
                        self.error(401)
                        return
                    else:
                        report.is_in_expensify = 1
                        report.put()
                        return self.response.out.write(True)
                else:
                    self.error(409) # is already in Expensify
                    return
            else:
                self.error(404) # report was not found
                return
        else:
            self.error(401) # unauthorized user
            return

    #Get all reports uploaded in Expensify
    #params: user_id
    def get(self):
        user_id = self.request.get('user_id')
        try:
            user_id = int(user_id)
        except ValueError:
            self.error(400)
            return
        expensify_user = ExpensifyUser.query(ExpensifyUser.user_id == user_id).get()
        if expensify_user:
            reports = []
            for report_key in expensify_user.report_keys:
                report = Report.get_by_id(report_key)
                reports.append(report)
            response = json.dumps(map(lambda report: report.convert_to_dict(), reports))
            self.response.out.write(response)
        else:
            self.error(401) # unauthorized user in Concur system

class ExpensifyUserHandler(webapp2.RequestHandler):
    """
    create user of Expensify in system
    json in body has to have 4 fields: user_email, user_expensify_secret, user_id, user_expensify_id
    """
    def post(self):
        info = json.loads(unicode(self.request.body, "utf-8"))
        user_id = info['user_id']
        expensify_id = info['user_expensify_id']
        expensify_secret = info['user_expensify_secret']
        email = info['user_email']
        try:
            user_id = int(user_id)
        except ValueError:
            self.error(400)
            return
        user = ExpensifyUser.query(ExpensifyUser.user_id == user_id).get()
        if user:
            user.key.delete()
        user = ExpensifyUser.create_user(user_id, expensify_id, expensify_secret, email)
        create_policy(user)
        user.put()
        self.response.out.write(True)
        return self.response.out.write(200)

    """
    send whether user has already integrated with Expensify
    """
    def get(self):
        user_id = self.request.get("user_id")
        try:
            user_id = int(user_id)
        except ValueError:
            self.error(400)
            return
        user = ExpensifyUser.query(ExpensifyUser.user_id == user_id).get()
        if user:
            self.response.out.write(True)
        else:
            self.response.out.write(False)


def create_policy(user):
    policy = ExpensifyUser.create_policy(user.expensify_user_id,user.expensify_user_secret)
    if (policy.status_code == 200):
        policy_info = json.loads(policy.content)
        user.policy_id = policy_info["policyID"]