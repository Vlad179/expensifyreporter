import sys
sys.path.insert(1, "C:\Program Files (x86)\Google\google_appenginee")
import unittest
import webapp2
import webtest
import json
import datetime
from entities import Report, ExpensifyUser, Check
from expensify_concurIntegration import ExpensifyRequestsHandler
from google.appengine.ext import testbed, ndb

class AppTest(unittest.TestCase):

    url = "/exp_user_send_report"
    #user in database
    user = ""


    def setUp(self):
        # Create a WSGI application.
        # Wrap the app with WebTests TestApp.
        app = webapp2.WSGIApplication([('/exp_user_send_report', ExpensifyRequestsHandler.ExpensifyReportHandler)], debug=True)
        self.testapp = webtest.TestApp(app)
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_urlfetch_stub()
        self.testbed.init_memcache_stub()
        ndb.get_context().clear_cache()

        self.user = ExpensifyUser.ExpensifyUser(user_id = 12345, user_email = "efanova.aleksandra@gmail.com",
                             expensify_user_id = "aa_efanova_aleksandra_gmail_com",
                             expensify_user_secret = "8eaed45b0657977c3aed9824c9544bc26c7c7287")
        self.user.policy_id = "8ED15441DF561019"
        ndb.put_multi([
            self.user
        ])


    def testSendReport_NotAuthorizedUser(self):
        data = { "user_id" : 1, "report_id" : 17895 }
        json_ = json.dumps(data).encode("utf-8")
        try:
            response = self.testapp.post(url=self.url, params=json_,content_type="application/json" )
        except Exception as ex:
            self.assertTrue(ex.message.__contains__("401"))

    def testSendReport_NoReport(self):
        data = { "user_id" : 12345, "report_id" : 1 }
        json_ = json.dumps(data).encode("utf-8")
        try:
            response = self.testapp.post(url=self.url, params=json_,content_type="application/json" )
        except Exception as ex:
            self.assertTrue(ex.message.__contains__("404"))

    def testSendReport_HasAlreadySuchReportInExpensify(self):
        report = Report.Report(user_id= 12345, name= "Report", initial_date= datetime.datetime.now())
        ndb.put_multi([
            report
        ])
        data = { "user_id" : 12345,"report_id" : report.key.id()}
        self.user.report_keys = [report.key.id()]
        json_ = json.dumps(data).encode("utf-8")
        try:
            response = self.testapp.post(url=self.url, params=json_,content_type="application/json" )
        except Exception as ex:
            self.assertTrue(ex.message.__contains__("409"))

    def testSendReport_WithoutExpenses_OK(self):
        report = Report.Report(user_id= 12345, name= "Report", initial_date= datetime.datetime.now())
        ndb.put_multi([
            report
        ])
        data = { "user_id" : 12345,"report_id" : report.key.id()}
        json_ = json.dumps(data).encode("utf-8")
        response = self.testapp.post(url=self.url, params=json_,content_type="application/json")
        info = json.loads(response.body)
        self.assertEqual(info["responseCode"], 200)

    def testSendReport_WithExpenses_OK(self):
        #create Report which will be sent to Expensify
        report = Report.Report(user_id= 12345, name= "Report", initial_date= datetime.datetime.now())
        #create Expenses for Report
        check1 = Check.Check(user_id = 12345)
        check1.currency = "USD"
        check1.date = datetime.datetime.today()
        check1.total = 234.34

        check2 = Check.Check(user_id = 12345)
        check2.currency = "RUB"
        check2.date = datetime.datetime(2013, 9, 30)
        check2.total = 13459.50

        ndb.put_multi([report])

        report.check_keys.append(check1)
        report.check_keys.append(check2)
        ndb.put_multi([check1, check2])
        #data sent to server
        data = { "user_id" : 12345,"report_id" : report.key.id()}
        json_ = json.dumps(data).encode("utf-8")
        response = self.testapp.post(url=self.url, params=json_,content_type="application/json")
        info = json.loads(response.body)
        self.assertEqual(info["responseCode"], 200)


    def tearDown(self):
        self.testbed.deactivate()