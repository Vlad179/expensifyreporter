currency = {
    "USD-US, Dollar",
           "RUB-Russia, Ruble (RUB)",
           "AFN-Afghanistan, Afghani",
           "ALL-Albania, Lek",
           "DZD-Algeria, Dinar",
           "AOA-Angola, Kwanza",
           "ARS-Argentina, Peso",
           "AMD-Armenia, Dram",
           "AWG-Aruba, Florin",
           "AUD-Australia, Dollar",
           "AZN-Azerbaijan, Manat",
           "BSD-Bahamas, Dollar",
           "BHD-Bahrain, Dinar",
           "BDT-Bangladesh, Taka",
           "BBD-Barbados, Dollar",
           "BYR-Belarus, Ruble",
           "BZD-Belize, Dollar",
           "BMD-Bermuda, Dollar",
           "BTN-Bhutan, Ngultrum",
           "BOB-Bolivia, Boliviano",
           "BWP-Botswana, Pula",
           "BAM-Boznia and Herzegovina, Convertible Marks",
           "BRL-Brazil, Real",
           "BND-Brunei, Dollar",
           "BGN-Bulgaria, Bulgarian Lev",
           "BIF-Burundi, Franc",
           "KHR-Cambodia, Riel",
           "CAD-Canada, Dollar",
           "CVE-Cape Verde, Escudo",
           "KYD-Cayman Islands, Dollar",
           "XOF-CFA Franc BCEAO",
           "XAF-CFA Franc BEAC",
           "XPF-CFP Franc",
           "CLP-Chile, Peso",
           "CNY-China, Yuan Renminbi",
           "COP-Colombia, Peso",
           "KMF-Comoros, Franc",
           "CDF-Congo, Franc Congolais",
           "CRC-Costa Rica, Colon",
           "HRK-Croatia, Kuna",
           "CUP-Cuba, Peso",
           "CUC-Cuba, Peso Convertible",
           "CZK-Czech Republic, Koruna",
           "DKK-Denmark, Krone",
           "DJF-Djibouti, Franc",
           "DOP-Dominican Republic, Peso",
           "XCD-East Caribbean Dollar",
           "EGP-Egypt, Pound",
           "ERN-Eritrea, Nakfa",
           "ETB-Ethiopia, Birr",
           "EUR-Euro",
           "FKP-Falkland Islands, Pound",
           "FJD-Fiji, Dollar",
           "GMD-Gambia, Dalasi",
           "GEL-Georgia, Lari",
           "GHS-Ghana, Cedi",
           "GIP-Gibraltar, Pound",
           "GTQ-Guatemala, Quetzal",
           "GNF-Guinea, Franc",
           "GYD-Guyana, Dollar",
           "HTG-Haiti, Gourde",
           "HNL-Honduras, Lempira",
           "HKD-Hong Kong, Dollar",
           "HUF-Hungary, Forint",
           "ISK-Iceland, Krona",
           "INR-India, Rupee",
           "IDR-Indonesia, Rupiah",
           "IRR-Iran, Rial",
           "IQD-Iraq, Dinar",
           "ILS-Israel, New Israeli Sheqel",
           "JMD-Jamaica, Dollar",
           "JPY-Japan, Yen",
           "JOD-Jordan, Dinar",
           "KZT-Kazakhstan, Tenge",
           "KES-Kenya, Shilling",
           "KPW-Korea (Democratic), North Korean Won",
           "KRW-Korea (Republic), Won",
           "KWD-Kuwait, Dinar",
           "KGS-Kyrgyzstan, Som",
           "LAK-Laos, Kip",
           "LBP-Lebanon, Pound",
           "LSL-Lesotho, Loti",
           "LRD-Liberia, Dollar",
           "LYD-Libya, Dinar",
           "MOP-Macao, Pataca",
           "MKD-Macedonia, Denar",
           "MGA-Madagascar, Ariary (MGA)",
           "MWK-Malawi, Kwacha",
           "MYR-Malaysia, Ringgit",
           "MVR-Maldives, Rufiyaa",
           "MRO-Mauritania, Ouguiya",
           "MUR-Mauritius, Rupee",
           "MXN-Mexico, Peso",
           "MDL-Moldova, Leu",
           "MNT-Mongolia, Tugrik",
           "MAD-Morocco, Dirham",
           "MZN-Mozambique, Metical",
           "MMK-Myanmar, Kyat",
           "NAD-Namibia, Dollar",
           "NPR-Nepal, Rupee",
           "ANG-Netherlands Antilles, Guilder",
           "NZD-New Zealand, Dollar",
           "NIO-Nicaragua, Cordoba Oro",
           "NGN-Nigeria, Naira",
           "NOK-Norway, Krone",
           "OMR-Oman, Rial Omani",
           "PKR-Pakistan, Rupee",
           "PAB-Panama, Balboa",
           "PGK-Papua New Guinea, Kina",
           "PYG-Paraguay, Guarani",
           "PEN-Peru, Nuevo Sol",
           "PHP-Philippines, Peso",
           "PLN-Poland, Zloty",
           "QAR-Qatar, Rial",
           "RON-Romania, Leu",
           "RWF-Rwanda, Franc",
           "SHP-Saint Helena, Pound",
           "WST-Samoa, Tala",
           "STD-Sao Tome and Principe, Dobra",
           "SAR-Saudi Arabia, Riyal",
           "RSD-Serbia, Dinar",
           "SCR-Seychelles, Rupee",
           "SLL-Sierra Leone, Leone",
           "SGD-Singapore, Dollar",
           "SBD-Solomon Islands, Dollar",
           "SOS-Somalia, Shilling",
           "ZAR-South Africa, Rand",
           "SSP-South Sudan, Pound",
           "LKR-Sri Lanka, Rupee",
           "SDG-Sudan, Pound",
           "SRD-Suriname, Dollar",
           "SZL-Swaziland, Lilangeni",
           "SEK-Sweden, Krona",
           "CHF-Switzerland, Franc",
           "SYP-Syria, Pound",
           "TWD-Taiwan, New Taiwan Dollar",
           "TJS-Tajikistan, Somoni",
           "TZS-Tanzania, Shilling",
           "THB-Thailand, Baht",
           "TOP-Tonga, Pa?anga",
           "TTD-Trinidad and Tobago, Dollar",
           "TND-Tunisia, Dinar",
           "TRY-Turkey, Lira",
           "TMT-Turkmenistan, New Manat",
           "AED-UAE, Dirham",
           "UGX-Uganda, Shilling",
           "GBP-UK, Pound Sterling",
           "UAH-Ukraine, Hryvnia",
           "UYU-Uruguay, Peso Uruguayo",
           "UZS-Uzbekistan, Sum",
           "VUV-Vanuatu, Vatu",
           "VEF-Venezuela, Bolivar",
           "VND-Viet Nam, Dong",
           "YER-Yemen, Rial",
           "ZMW-Zambia, Kwacha"
}

def set_currency_for_integration(current_currency):
    if current_currency in currency:
        return current_currency[:3]
    raise Exception('Incorrect currency')