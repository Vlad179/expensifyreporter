from google.appengine.ext import ndb
import datetime
import webapp2
import json


from RequestBuilder import RequestBuilder
from entities.ConcurUser import ConcurUser
from entities.Report import Report


class ConcurReportHandler(webapp2.RequestHandler):
    #Upload report in Concur
    #fields: report_id, user_id
    def post(self):
        info = json.loads(unicode(self.request.body, "utf-8"))
        user_id = info["user_id"]
        report_id = info["report_id"]
        try:
            user_id = int(user_id)
            report_id = long(report_id)
        except ValueError:
            self.error(400)
            return
        user = ConcurUser.query(ConcurUser.user_id == user_id).get()
        if user:
            #check whether the token is expired
            if datetime.datetime.now() > user.expiration_date:
                refreshed = RequestBuilder.refresh_access_token(user)
                if not refreshed:
                    self.error(500) #internal server error
                    return
            report = Report.get_by_id(report_id)
            if report:
                report_key_list = user.report_keys
                if report_id: #not in report_key_list:
                    user.report_keys.append(report_id)
                    result = RequestBuilder.concur_create_report(user, report.name,
                                                      report.initial_date, report.check_keys)
                    user.put()
                    report.is_in_concur = 1
                    report.put()
                    return self.response.out.write(True)
                else: #check whether this report was deleted in Concur
                    RequestBuilder.get_all_user_reports_in_concur(user) #update list of reports in Concur
                    report_key_list = user.report_keys
                    if report_id not in report_key_list:
                        user.report_keys.append(report_id)
                        user.put()
                        result = RequestBuilder.concur_create_report(user, report.name,
                                                      report.initial_date, report.check_keys)
                        report.is_in_concur = 1
                        return self.response.out.write(True)
                    else:
                        self.error(409) # this report is already in Concur
                        return
            else:
                self.error(404) # report was not found
                return
        else:
            self.error(401) # unauthorized user in Concur system
            return

    #Get all reports uploaded in Concur
    #params: user_id
    def get(self):
        user_id = self.request.get('user_id')
        try:
            user_id = int(user_id)
        except ValueError:
            self.error(400)
            return
        concur_user = ConcurUser.query(ConcurUser.user_id == user_id).get()
        if concur_user:
            RequestBuilder.get_all_user_reports_in_concur(concur_user)
            reports = []
            for report_key in concur_user.report_keys:
                report = Report.get_by_id(report_key)
                reports.append(report)
            response = json.dumps(map(lambda report: report.convert_to_dict(), reports))
            self.response.out.write(response)
        else:
            self.error(401) # unauthorized user in Concur system


class ConcurUserHandler(webapp2.RequestHandler):
    """
    create user of Concur in system
    json in body has to have 4 fields: user_id, access_token, expiration_date, refresh_token
    """
    def post(self):
        info = json.loads(unicode(self.request.body, "utf-8"))
        user_id = info['user_id']
        access_token = info['access_token']
        expiration_date = info['expiration_date']
        refresh_token = info['refresh_token']
        try:
            user_id = int(user_id)
            expiration_date = datetime.datetime.strptime(expiration_date, "%m/%d/%Y %H:%M:%S")
        except ValueError:
            self.error(400)
            return
        user = ConcurUser.query(ConcurUser.user_id == user_id).get()
        if user:
            user.key.delete()
        user = ConcurUser.create_user(user_id, access_token, expiration_date, refresh_token)
        user.put()
        self.response.out.write(True)
        return self.response.out.write(200)

    """
    send whether user has already integrated with Concur
    """
    def get(self):
        user_id = self.request.get("user_id")
        try:
            user_id = int(user_id)
        except ValueError:
            self.error(400)
            return
        user = ConcurUser.query(ConcurUser.user_id == user_id).get()
        if user:
            self.response.out.write(True)
        else:
            self.response.out.write(False)

