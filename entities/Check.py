import base64

from google.appengine.ext import ndb
from google.appengine.ext.blobstore import blobstore

__author__ = 'Andrey Kudryavtsev, Vladislav Bezhentsev'


class Check(ndb.Model):
    user_id = ndb.IntegerProperty(required=True)
    photo = ndb.BlobKeyProperty()
    total = ndb.FloatProperty(default=0.0)
    date = ndb.DateTimeProperty(auto_now_add=True)
    category = ndb.StringProperty()
    address = ndb.StringProperty()
    note = ndb.StringProperty()
    currency = ndb.StringProperty()  # necessary for Expensify
    related_reports = ndb.IntegerProperty(repeated=True)

    def convert_to_dict(self, del_photo=True):
        d = self.to_dict()
        d['date'] = d['date'].isoformat()
        d['key'] = self.key.urlsafe()
        if del_photo:
            del d['photo']
        else:
            try:
                blob_reader = blobstore.BlobReader(d['photo'])
                d['photo'] = base64.b64encode(blob_reader.read())
            except ValueError:
                d['photo'] = ''
        return d
