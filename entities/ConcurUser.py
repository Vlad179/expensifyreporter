from google.appengine.ext import ndb
from entities import Report
import datetime

import json
import requests


class ConcurUser(ndb.Model):
    user_id = ndb.IntegerProperty(required=True)
    access_token = ndb.StringProperty(required=True)
    expiration_date = ndb.DateTimeProperty(required=True)
    refresh_token = ndb.StringProperty(required=True)
    report_keys = ndb.IntegerProperty(repeated=True)
    concur_report_ids = ndb.StringProperty(repeated=True)

    @staticmethod
    def create_user(user_id, access_token, expiration_date, refresh_token):
        con_user = ConcurUser(user_id = user_id, access_token = access_token,
                              expiration_date = expiration_date,
                                 refresh_token = refresh_token)
        con_user.put()
        return con_user