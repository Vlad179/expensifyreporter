import unittest
import webapp2
import webtest
import json
import datetime
from entities import Report, ExpensifyUser
from expensify_concurIntegration import ExpensifyRequestsHandler
from google.appengine.ext import testbed, ndb

class AppTest(unittest.TestCase):
    def setUp(self):
        # Create a WSGI application.
        app = webapp2.WSGIApplication([('/exp_user', ExpensifyRequestsHandler.ExpensifyUserHandler)], debug=True)
        # Wrap the app with WebTests TestApp.
        self.testapp = webtest.TestApp(app)
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_urlfetch_stub()
        self.testbed.init_memcache_stub()
        ndb.get_context().clear_cache()

    # Test the creation of exp user in the system.
    def testCreateUSerHandler(self):
        header = {"user_id" : 12345,
                    "user_expensify_id"  : "aa_efanova_aleksandra_gmail_com",
                    "user_expensify_secret" : "8eaed45b0657977c3aed9824c9544bc26c7c7287",
                    "user_email" : "efanova.aleksandra" + u"\u0040" + "gmail.com"}
        json_ = json.dumps(header).encode("utf-8")
        response = self.testapp.post(url="/exp_user", params=json_,content_type="application/json" )
        self.assertEqual(response.status_int, 200)
        print response

    def tearDown(self):
        self.testbed.deactivate()