import json

import webapp2
import requests
import dateutil.parser

from entities.Check import Check
from entities.Report import Report
from google.appengine.ext import ndb
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext.blobstore import blobstore

from requests.exceptions import ConnectionError

__author__ = 'Bezhentsev Vladislav, Andrey Kudryavtsev'


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        upload_files = self.get_uploads('photo')

        if len(upload_files) > 0:
            blob_info = upload_files[0]
            self.response.write(str(blob_info.key()))
        else:
            self.error(404)


class ChecksRequestHandler(webapp2.RequestHandler):
    def get(self):
        checks = Check.query().order(-Check.date)
        start = self.request.get('start', 0)
        end = self.request.get('end', checks.count())
        user_id = self.request.get('user_id')
        start_date = self.request.get('start_date')
        end_date = self.request.get('end_date')
        cat = self.request.get('category')
        name_fragment = self.request.get('name')

        try:
            start = int(start)
            if start < 0:
                raise ValueError('The value of parameter \'start\' should be a non-negative integer')
            end = int(end)
            if end < 0:
                raise ValueError('The value of parameter \'end\' should be a non-negative integer')
            user_id = int(user_id)
            if user_id <= 0:
                raise ValueError('The value of parameter \'user_id\' should be a positive integer')
            start_date = dateutil.parser.parse(start_date) if start_date else ''
            end_date = dateutil.parser.parse(end_date) if end_date else ''
        except ValueError:
            self.error(400)
            return

        result = checks.fetch(offset=start, limit=end - start) if end > start else []
        result = filter(lambda check: check.user_id == user_id, result)
        if cat:
            result = filter(lambda check: check.category == cat, result)
        result = filter(lambda check: name_fragment in u' '.join(check.note if check.note else '').encode('utf-8'), result)
        if start_date != '':
            result = filter(lambda check: check.date >= start_date, result)
        if end_date != '':
            result = filter(lambda check: check.date <= end_date, result)

        response = json.dumps(map(lambda check: check.convert_to_dict(del_photo=False), result))
        self.response.out.write(response)

    def post(self):
        user_id = self.request.get('user_id')
        total = self.request.get('total')
        date = self.request.get('date')
        category = self.request.get('category')
        address = self.request.get('address')
        note = self.request.get('note')
        currency = self.request.get('currency')
        check_key = self.request.get('check_key')
        need_recognition = self.request.get('recognize')
        photo = self.request.POST.get('photo')

        try:
            user_id = int(user_id)
            if user_id <= 0:
                raise ValueError('The value of parameter \'user_id\' should be a positive integer')
            if total != '':
                total = float(total)
                if total < 0:
                    raise ValueError('The value of parameter \'total\' should be a non-negative number')
            if date != '':
                date = dateutil.parser.parse(date)
            check_key = ndb.Key(urlsafe=check_key) if check_key != '' else ''
            if need_recognition == '':
                need_recognition = 0
            else :
                need_recognition= int(need_recognition)
            need_recognition = False if (need_recognition == 0 or need_recognition == "") else True
        except ValueError:
            self.error(400)
            return

        if check_key == '':
            check = Check(user_id=user_id)
            check.put()
        else:
            check = check_key.get()

        if photo is not None:
            files = {'photo': ('photo.jpg', photo.file.read(), 'image/jpeg')}
            result = requests.post(blobstore.create_upload_url('/upload'), files=files)

            if result.status_code == 200:
                blob_key_str = result.content
                blob_key = blobstore.BlobKey(blob_key_str)
                check.photo = blob_key

                if need_recognition:
                    try:
                        resp = requests.post('http://104.131.116.142:5000/', files=files)
                        check.total = float(resp.content.strip())
                    except (ConnectionError, ValueError):
                        self.error(500)
                        return

        if total != '':
            delta = total - check.total
            check.total = total
            for report_id in check.related_reports:
                report = Report.get_by_id(report_id)
                report.total += delta
                report.put()
        if date != '':
            check.date = date
        if category != '':
            check.category = category
        if address != '':
            check.address = address
        if note != '':
            check.note = note
        if currency != '':
            check.currency = currency
        check.put()

        response = json.dumps(check.convert_to_dict())
        self.response.out.write(response)

    def delete(self):
        check_key = self.request.get('check_key')

        try:
            check_key = ndb.Key(urlsafe=check_key)
        except ValueError:
            self.error(400)
            return

        check = check_key.get()
        if check:
            for report_id in check.related_reports:
                report = Report.get_by_id(report_id)
                report.remove_checks([check_key])
                report.total -= check.total
                report.put()
            check_key.delete()
