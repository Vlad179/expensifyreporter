from google.appengine.ext import ndb
from google.appengine.api import urlfetch
from entities import Report
import urllib
import json


class ExpensifyUser(ndb.Model):
    user_id = ndb.IntegerProperty(required=True)
    user_email = ndb.StringProperty(required=True)
    expensify_user_id = ndb.StringProperty(required=True)
    expensify_user_secret = ndb.StringProperty(required=True)
    policy_id = ndb.StringProperty(repeated=False)
    report_keys = ndb.IntegerProperty(repeated=True)

    @staticmethod
    def create_user(user_id, user_expensify_id, user_expensify_secret, user_email):
        exp_user = ExpensifyUser(user_id = user_id, user_email = user_email, expensify_user_id = user_expensify_id,
                                 expensify_user_secret = user_expensify_secret)
        exp_user.put()
        return exp_user

    @staticmethod
    def create_policy(user_expensify_id, user_expensify_secret):
        expensify_url = "https://integrations.expensify.com/Integration-Server/ExpensifyIntegrations?requestJobDescription="
        data = {"type":"create"}
        credentials = {}
        credentials['partnerUserID'] = user_expensify_id
        credentials['partnerUserSecret'] = user_expensify_secret
        data["credentials"] = credentials
        inputSettings = {}
        inputSettings['type'] = "policy"
        inputSettings['policyName'] = "User's policy"
        data["inputSettings"] = inputSettings
        json_ = str(json.dumps(data))
        form_data = urllib.quote_plus(json_)
        url = '%s%s' % (expensify_url, form_data)
        result = urlfetch.fetch(url)
        print result.content
        return result